Categories:Navigation
License:GPLv3
Web Site:http://jkliemann.de/parkendd
Source Code:https://github.com/jklmnn/ParkenDD
Issue Tracker:https://github.com/jklmnn/ParkenDD/issues
#FlattrID:https://flattr.com/thing/752f3c69bfdeca06686f730348e93fdd
FlattrID:752f3c69bfdeca06686f730348e93fdd
Bitcoin:1HBFDdz7dJeBE2c3zQmWnzeWCbHoZQ64dd

Auto Name:ParkenDD
Summary:Show available parking slots in Dresden
Description:
Shows available parking lots in Dresden, Germany. Note that the
homepage is only available in German at the moment. The app is
translated into English and a few other languages. Further
languages will come.
.

Repo Type:git
Repo:https://github.com/jklmnn/ParkenDD

Build:0.1.0,1
    commit=71fdddb4509930af0b4fc202e9b42e18ac0f1525
    subdir=app
    gradle=yes

Build:0.1.1,2
    commit=v0.1.1
    subdir=app
    gradle=yes

Build:0.2.0,3
    commit=v0.2.0
    subdir=app
    gradle=yes

Build:0.2.1,4
    commit=v0.2.1
    subdir=app
    gradle=yes

Build:0.3.0,5
    commit=v0.3.0
    subdir=app
    gradle=yes

Build:0.3.1,6
    commit=v0.3.1
    subdir=app
    gradle=yes

Build:0.3.2,7
    commit=v0.3.2
    subdir=app
    gradle=yes

Build:0.3.3,8
    commit=v0.3.3
    subdir=app
    gradle=yes

Build:0.4.0,9
    commit=v0.4.0
    subdir=app
    gradle=yes

Build:0.5.0,10
    commit=v0.5.0
    subdir=app
    gradle=yes

Build:0.5.1,11
    commit=v0.5.1
    subdir=app
    gradle=yes

Build:0.6.0-test,12
    commit=v0.6.0-test
    subdir=app
    gradle=yes

Build:0.6.1-test,13
    commit=v0.6.1-test
    subdir=app
    gradle=yes

Build:0.7.0-test,14
    commit=v0.7.0-test
    subdir=app
    gradle=yes

Build:0.7.1-test,15
    commit=v0.7.1-test
    subdir=app
    gradle=yes

Build:0.7.2-test,16
    commit=v0.7.2-test
    subdir=app
    gradle=yes

Build:0.7.3-test,17
    commit=v0.7.3-test
    subdir=app
    gradle=yes

Build:0.7.4-test,18
    commit=v0.7.4-test
    subdir=app
    gradle=yes

Build:0.8.0,19
    commit=v0.8.0
    subdir=app
    gradle=yes

Build:0.8.1,20
    commit=v0.8.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.8.1
Current Version Code:20

