Categories:Science & Education
License:GPLv3
Web Site:https://github.com/ligi/VaporizerControl/blob/HEAD/README.md
Source Code:https://github.com/ligi/VaporizerControl
Issue Tracker:https://github.com/ligi/VaporizerControl/issues

Auto Name:Vaporizer Control Crafty
Summary:Control vaporizers via BLE
Description:
Control your vaporizer ( currently only crafty ) with this app.
.

Repo Type:git
Repo:https://github.com/ligi/VaporizerControl

Build:1.2,12
    commit=1.2
    subdir=mobile
    gradle=yes
    rm=wear
    prebuild=sed -i -e '/android-sdk-manager/d' -e '/play-services/d' build.gradle && \
        echo "include ':mobile'" > ../settings.gradle

Build:1.5,15
    commit=1.5
    subdir=mobile
    gradle=yes
    rm=wear
    prebuild=sed -i -e '/android-sdk-manager/d' -e '/play-services/d' build.gradle && \
        echo "include ':mobile'" > ../settings.gradle

Build:1.6,16
    commit=1.6
    subdir=mobile
    gradle=yes
    rm=wear
    prebuild=sed -i -e '/android-sdk-manager/d' -e '/play-services/d' build.gradle && \
        echo "include ':mobile'" > ../settings.gradle

Build:1.7,17
    commit=1.7
    subdir=mobile
    gradle=yes
    rm=wear
    prebuild=sed -i -e '/android-sdk-manager/d' -e '/play-services/d' build.gradle && \
        echo "include ':mobile'" > ../settings.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.7
Current Version Code:17

