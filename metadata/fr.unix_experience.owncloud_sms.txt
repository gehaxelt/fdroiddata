Categories:Phone & SMS
License:AGPLv3
Web Site:https://github.com/nerzhul/ownCloud-SMS-App/blob/HEAD/README.md
Source Code:https://github.com/nerzhul/ownCloud-SMS-App
Issue Tracker:https://github.com/nerzhul/ownCloud-SMS-App/issues

Auto Name:ownCloud-SMS
Summary:Synchronize your SMS messages with ownCloud
Description:
App to synchronize your SMS messages on a remote ownCloud instance and let you
read your messages from it.

Sending SMS from ownCloud instance will coming in a future release.

Application is fully compatible from Android 4.0 to 5.0

Note:
This app needs an ownCloud installation running the [http://apps.owncloud.com/content/show.php/ownCloud+SMS?content=167289 ocsms] app.
.

Repo Type:git
Repo:https://github.com/nerzhul/ownCloud-SMS-App.git

Build:0.16.1,17
    commit=6480b3dbbdc1474502697f1d53a51627ad052c5b
    srclibs=1:Support/v7/appcompat@android-4.4.2_r2,2:oc-android-library@e87f5f25ad91950d47ec9b6fa01401360cd7ec8d,slf4j@v_1.7.5,jackrabbit@2.7.2,tomcat-servlet-api@TOMCAT_8_0_0
    extlibs=android/android-support-v4.jar
    prebuild=pushd $$oc-android-library$$/libs && \
        rm jackrabbit*jar slf4j*jar && \
        popd && \
        mkdir -p $$Support$$/libs && \
        mv libs/android-support-v4.jar $$Support$$/libs && \
        mkdir -p $$oc-android-library$$/src/javax && \
        pushd $$jackrabbit$$/jackrabbit-webdav && \
        $$MVN3$$ package && \
        popd && \
        cp $$jackrabbit$$/jackrabbit-webdav/target/jackrabbit-webdav-2.7.2.jar $$oc-android-library$$/libs/ && \
        pushd $$slf4j$$/slf4j-api && \
        $$MVN3$$ package -DskipTests && \
        popd && \
        cp $$slf4j$$/slf4j-api/target/slf4j-api-1.7.5.jar $$oc-android-library$$/libs/
    target=android-19

Build:0.17.0,18
    commit=0.17.0
    srclibs=1:Support/v7/appcompat@android-4.4.2_r2,2:oc-android-library@e87f5f25ad91950d47ec9b6fa01401360cd7ec8d,slf4j@v_1.7.5,jackrabbit@2.7.2,tomcat-servlet-api@TOMCAT_8_0_0
    extlibs=android/android-support-v4.jar
    prebuild=pushd $$oc-android-library$$/libs && \
        rm jackrabbit*jar slf4j*jar && \
        popd && \
        mkdir -p $$Support$$/libs && \
        mv libs/android-support-v4.jar $$Support$$/libs && \
        mkdir -p $$oc-android-library$$/src/javax && \
        pushd $$jackrabbit$$/jackrabbit-webdav && \
        $$MVN3$$ package && \
        popd && \
        cp $$jackrabbit$$/jackrabbit-webdav/target/jackrabbit-webdav-2.7.2.jar $$oc-android-library$$/libs/ && \
        pushd $$slf4j$$/slf4j-api && \
        $$MVN3$$ package -DskipTests && \
        popd && \
        cp $$slf4j$$/slf4j-api/target/slf4j-api-1.7.5.jar $$oc-android-library$$/libs/
    target=android-19

Build:0.17.1,19
    commit=0.17.1
    srclibs=1:Support/v7/appcompat@android-4.4.2_r2,2:oc-android-library@e87f5f25ad91950d47ec9b6fa01401360cd7ec8d,slf4j@v_1.7.5,jackrabbit@2.7.2,tomcat-servlet-api@TOMCAT_8_0_0
    extlibs=android/android-support-v4.jar
    prebuild=pushd $$oc-android-library$$/libs && \
        rm jackrabbit*jar slf4j*jar && \
        popd && \
        mkdir -p $$Support$$/libs && \
        mv libs/android-support-v4.jar $$Support$$/libs && \
        mkdir -p $$oc-android-library$$/src/javax && \
        pushd $$jackrabbit$$/jackrabbit-webdav && \
        $$MVN3$$ package && \
        popd && \
        cp $$jackrabbit$$/jackrabbit-webdav/target/jackrabbit-webdav-2.7.2.jar $$oc-android-library$$/libs/ && \
        pushd $$slf4j$$/slf4j-api && \
        $$MVN3$$ package -DskipTests && \
        popd && \
        cp $$slf4j$$/slf4j-api/target/slf4j-api-1.7.5.jar $$oc-android-library$$/libs/
    target=android-19

Build:0.17.3,22
    commit=29e67d11090dbe65d55b1bb294c1e00a3b18a6cc
    srclibs=1:Support/v7/appcompat@android-4.4.2_r2,2:oc-android-library@e87f5f25ad91950d47ec9b6fa01401360cd7ec8d,slf4j@v_1.7.5,jackrabbit@2.7.2,tomcat-servlet-api@TOMCAT_8_0_0
    extlibs=android/android-support-v4.jar
    prebuild=pushd $$oc-android-library$$/libs && \
        rm jackrabbit*jar slf4j*jar && \
        popd && \
        mkdir -p $$Support$$/libs && \
        mv libs/android-support-v4.jar $$Support$$/libs && \
        mkdir -p $$oc-android-library$$/src/javax && \
        pushd $$jackrabbit$$/jackrabbit-webdav && \
        $$MVN3$$ package && \
        popd && \
        cp $$jackrabbit$$/jackrabbit-webdav/target/jackrabbit-webdav-2.7.2.jar $$oc-android-library$$/libs/ && \
        pushd $$slf4j$$/slf4j-api && \
        $$MVN3$$ package -DskipTests && \
        popd && \
        cp $$slf4j$$/slf4j-api/target/slf4j-api-1.7.5.jar $$oc-android-library$$/libs/
    target=android-19

Build:0.18.2,26
    commit=0.18.2
    forceversion=yes
    forcevercode=yes
    srclibs=1:Support/v7/appcompat@android-4.4.2_r2,2:oc-android-library@oc-android-library-0.9.6,slf4j@v_1.7.5,jackrabbit@2.7.2,tomcat-servlet-api@TOMCAT_8_0_0,nrz-androidlib@cfa7a225af2ee65e5fe09023466fd8258a1a6d48
    extlibs=android/android-support-v4.jar,android/android-support-v13.jar
    rm=libs/android-support-v13.jar
    prebuild=pushd $$oc-android-library$$/libs && \
        rm jackrabbit*jar slf4j*jar && \
        popd && \
        mkdir -p $$Support$$/libs && \
        mv libs/android-support-v4.jar $$Support$$/libs && \
        mkdir -p $$oc-android-library$$/src/javax && \
        pushd $$jackrabbit$$/jackrabbit-webdav && \
        $$MVN3$$ package && \
        popd && \
        cp $$jackrabbit$$/jackrabbit-webdav/target/jackrabbit-webdav-2.7.2.jar $$oc-android-library$$/libs/ && \
        pushd $$slf4j$$/slf4j-api && \
        $$MVN3$$ package -DskipTests && \
        popd && \
        cp $$slf4j$$/slf4j-api/target/slf4j-api-1.7.5.jar $$oc-android-library$$/libs/ && \
	cp -fR $$nrz-androidlib$$/src/* src/
    target=android-22

Maintainer Notes:
$$oc-android-library$$/libs/ should be build from source. Currently
commons-httpclient is missing: Wasn't able to build this specific
version.

Tag 0.18.2 currently doesn't conntain the correct VC/VN
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.18.2
Current Version Code:26

