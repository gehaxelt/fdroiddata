Categories:Office
License:Apache2
Web Site:https://github.com/alexghr/bulkshare/blob/HEAD/README.md
Source Code:https://github.com/alexghr/bulkshare
Issue Tracker:https://github.com/alexghr/bulkshare/issues

Auto Name:Bulk Share
Summary:Share multiple URLs
Description:
Select multiple URLs to send via email, SMS, etc.
.

Repo Type:git
Repo:https://github.com/alexghr/bulkshare

Build:1.0,1
    commit=v1.0.0
    subdir=app
    gradle=yes

Build:1.1.0,2
    commit=v1.1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.0
Current Version Code:2

